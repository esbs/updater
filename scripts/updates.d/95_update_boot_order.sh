#!/bin/sh
# SPDX-License-Identifier: GPL-2.0-or-later
#
# Copyright (C) 2020 EVBox B.V.
# Copyright (C) 2020 Thibault Ferrante <thibault.ferrante@pm.me>

set -eu

DD_BLOCKSIZE=1048576  # 1 MiB seems reasonable
REQUIRED_COMMANDS="
	[
	break
	cmp
	command
	dd
	echo
	exit
	getopts
	seq
	set
	shift
	test
"


e_err()
{
	>&2 echo "ERROR: ${*}"
}

e_warn()
{
	echo "WARN: ${*}"
}

usage()
{
	echo "Usage: ${0} [OPTIONS]"
	echo
	echo "Update the boot order following platform information"
	echo "    -h Print this help text and exit"
	echo
	echo "Warning: This script is destructive and can destroy your data."
}

check_requirements()
{
	for _cmd in ${REQUIRED_COMMANDS}; do
		if ! _test_result="$(command -V "${_cmd}")"; then
			_test_result_fail="${_test_result_fail:-}${_test_result}\n"
		else
			_test_result_pass="${_test_result_pass:-}${_test_result}\n"
		fi
	done

	if [ -n "${_test_result_fail:-}" ]; then
		e_err "Self-test failed, missing dependencies."
		echo "======================================="
		echo "Passed tests:"
		# shellcheck disable=SC2059  # Interpret \n from variable
		printf "${_test_result_pass:-none\n}"
		echo "---------------------------------------"
		echo "Failed tests:"
		# shellcheck disable=SC2059  # Interpret \n from variable
		printf "${_test_result_fail:-none\n}"
		echo "======================================="
		exit 1
	fi
}

invalidate_partition()
{
	_target_device="${1?Missing argument to function}"
	_rom_bank="${2?Missing argument to function}"

	_partition="$(sfdisk --dump --quiet "${_target_device}" | \
	              sed -n 's|^[[:space:]]*\(/dev/[[:alnum:]]\+\)[[:space:]]*:.*name=[[:space:]]*\"\?'"${_rom_bank}"'\"\?.*$|\1|p')"

	if [ -z "${_partition}" ]; then
		e_err "Disk '${_target_device}' does not contain a partition named '${_rom_bank}', cannot erase"
		return
	fi

	for _ in $(seq 1 10); do
		if ! dd \
		        bs="${DD_BLOCKSIZE?}" \
		        conv="fsync,notrunc" \
		        if="/dev/zero" \
		        of="${_partition}" \
		        count=1 \
		        status=none \
		        1> "/dev/null"; then
			e_warn "Failure erasing '${_partition}', retrying"
			continue
		fi
		echo "Erased partition'${_partition}'"
		return
	done
	e_warn "Failure erasing '${_partition}'"
}

main()
{
	while getopts ":b:d:fh" options; do
		case "${options}" in
		b)
			target_bank="${OPTARG}"
			;;
		d)
			target_device="${OPTARG}"
			;;
		f)
			echo "Failure flag not implemented for '${0}', ignoring"
			;;
		h)
			usage
			exit 0
			;;
		:)
			e_err "Option -${OPTARG} requires an argument."
			exit 1
			;;
		?)
			e_err "Invalid option: -${OPTARG}"
			exit 1
			;;
		esac
	done
	shift "$((OPTIND - 1))"

	check_requirements

	if ! "boot_control.sh" -s; then
		e_err "Unable to update the boot order, invalidating the target bank before exit"
		e_err "Press ctrl-c to abort now"
		sleep 3;

		e_err "Invalidating 'boot_${target_bank}' and 'rom_${target_bank}'"
		invalidate_partition "${target_device}" "boot_${target_bank}"
		invalidate_partition "${target_device}" "rom_${target_bank}"
		exit 1
	fi
}

main "${@}"

exit 0
