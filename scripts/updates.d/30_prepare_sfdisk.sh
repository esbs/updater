#!/bin/sh
# SPDX-License-Identifier: GPL-2.0-or-later
#
# Copyright (C) 2018 Ultimaker B.V.
# Copyright (C) 2019 EVBox B.V.
# Copyright (C) 2018 Olliver Schinagl <oliver@schinagl.nl>
# Copyright (C) 2018 Raymond Siudak <raysiudak@gmail.com>

set -eu

COMPATIBLES_PATH="/sys/firmware/devicetree/base/compatible"
DEVICE_PARTITION_POSTFIXES="boot0 boot1 gp0 gp1 gp2 gp3 rpmb"
PARTITION_EXT4_SIZE_MIN=16384 # 8MiB * 512 bytes per sector is our useful minimum
PARTITION_F2FS_SIZE_MIN=78124 # ~38MiB is the minimum for F2FS
SYSTEM_UPDATE_ETC_DIR="/etc/system_update/"
REQUIRED_COMMANDS="
	[
	blkid
	command
	continue
	echo
	exit
	fsck.ext4
	fsck.f2fs
	getopts
	grep
	mkfs.ext4
	mkfs.f2fs
	mkswap
	mount
	mountpoint
	partprobe
	printf
	readlink
	resize.f2fs
	resize2fs
	sed
	seq
	set
	sfdisk
	sha512sum
	shift
	stat
	test
	umount
	unset
"
REQUIRED_FILESYSTEMS="
	ext4
	f2fs
"


e_err()
{
	>&2 echo "ERROR: ${*}"
}

e_warn()
{
	echo "WARN: ${*}"
}

usage()
{
	echo "Usage: ${0} [OPTIONS] UPDATE_FILE"
	echo "Prepare the target storage device to a predefined disk layout."
	echo "    -d Mandatory target device"
	echo "    -f Force failures where failures would be accepted (ex. partprobe)"
	echo "    -h Print this help text and exit"
	echo "    -t Partition table (default: '\${TARGET_DEVICE}.sfdisk')"
	echo
	echo "UPDATE_FILE, software update file to update with."
	echo "The default partition table file is determined from the storage device."
	echo "Additional hardware partitions (boot[01], gp[0-3], rpmb) need sfdisk files."
	echo "Warning: This script is destructive and can destroy your data."
}

is_integer()
{
	test "${1}" -eq "${1}" 2> "/dev/null"
}

is_comment()
{
	test -z "${1%%#*}"
}

init()
{
	update_file_sectors="$((($(stat -c "%s" "${update_file}") / 512) + 1))"

	if [ ! -b "${target_device:-}" ]; then
		e_err "Target storage device '${target_device:-}' is not a block device"
		exit 1
	fi
}

partition_table_extract()
{
	_partition_table="${1?Missing argument to function}"

	# Produces a comma separated line containing only the device node, start
	# integer, size integer and name of a partition whilst ignoring any leading
	# zero's or empty partitions (size=0), e.g. "/dev/loop0p1,2048,16384,boot".
	echo "${_partition_table}" | while read -r line; do
		printf "%s" "${line}" | sed -n 's|^.*p.*\([[:digit:]]\+\)[[:space:]]:.*$|p\1,|p'
		printf "%s" "${line}" | sed -n 's|^.*start=[[:space:]]*\([1-9][[:digit:]]*\).*$|\1,|p'
		printf "%s" "${line}" | sed -n 's|^.*size=[[:space:]]*0*\([1-9][[:digit:]]*\).*$|\1,|p'
		printf "%s" "${line}" | sed -n 's|^.*name=[[:space:]]*\"\?\([[:alnum:]_]\+\)\"\?.*$|\1\n|p'
	done
}

partitions_sync()
{
	_target_device="${1?Missing argument to function}"

	for _ in $(seq 1 5); do
		if partprobe "${_target_device}"; then
			return 0
		fi

		e_warn "Partprobe failed for '${_target_device}', this can happen on slow media, retrying"
		sleep 1
	done

	if [ "${force_failure:-}" = "true" ]; then
		e_err "Unable to partprobe '${_target_device}', giving up."

		return 1
	fi

	e_warn "Unable to properly partprobe '${_target_device}', continuing anyway."
}

partitions_format()
{
	_target_device="${1?Missing argument to function}"
	_partition_file="${2?Missing argument to function}"

	_partition_table_disk_data="$(partition_table_extract "$(sfdisk --dump --quiet "${_target_device}" | sort -n)")"
	_partition_table_file_data="$(partition_table_extract "$(sort -n < "${_partition_file}")")"

	while IFS="," read -r _disk_partition _disk_start _; do
		while IFS="," read -r _table_partition _table_start _ _table_name _; do
			unset _active_disk_unmounted
			if [ -z "${_disk_start}" ] || [ -z "${_table_start}" ] || \
			   [ "${_disk_start}" != "${_table_start}" ]; then
				continue
			fi

			_target_partition="${_target_device}${_disk_partition}"
			if [ -L "${_target_partition}" ]; then
				_target_partition="$(readlink -f "${_target_partition}")"
			fi

			if [ ! -b "${_target_partition}" ]; then
				e_err "'${_target_partition}' is not a block device, cannot continue"
				exit 1
			fi

			if [ -z "${_table_name}" ]; then
				e_err "partition name for '${_target_device}${_table_partition}' is empty"
				exit 1
			fi

			for _ in $(seq 1 3); do
				if grep -q "^${_target_partition} " "/proc/mounts" && \
 				   ! umount "${_target_partition}"; then
					continue
				fi

				if ! grep -q "^${_target_partition} " "/proc/mounts"; then
					_active_disk_unmounted="true"
					break
				fi

				e_warn "Failed to unmount the target '${_target_partition}', this can happen on slow media, retrying"
				sleep 1
			done

			if [ "${_active_disk_unmounted:-}" != "true" ]; then
				e_warn "Unable to unmount the target '${_target_partition}', skipping"
				continue
			fi

			# If the partition was already valid, just re-size the existing one.
			# If fsck or re-size fails, reformat.
			if _fstype="$(blkid "${_target_partition}" | \
			              sed -n 's|^.*TYPE=\"\?\([[:alnum:]]\+\)\"\?.*$|\1|p')"; then
				unset _format_needed
				unset _fsck_cmd
				unset _fsck_ret_ok
				unset _mkfs_cmd
				case "${_fstype}" in
				ext4)
					_fsck_cmd="fsck.ext4 -f -y"
					_fsck_ret_ok=1
					_mkfs_cmd="mkfs.ext4 -F -L ${_table_name} -O ^metadata_csum"
					_resize_cmd="resize2fs"
					;;
				f2fs)
					_fsck_cmd="fsck.f2fs -f -p -y"
					_fsck_ret_ok=0
					_mkfs_cmd="mkfs.f2fs -f -l ${_table_name}"
					_resize_cmd="resize.f2fs"
					;;
				swap)
					_fsck_cmd="/bin/false"
					_fsck_ret_ok=0
					_mkfs_cmd="mkswap -L ${_table_name}"
					_resize_cmd="${_mkfs_cmd}"
					;;
				*)
					if [ "${_table_name#boot*}" != "${_table_name}" ]; then
						_mkfs_cmd="mkfs.ext4 -F -L ${_table_name} -O ^metadata_csum"
						_format_needed="true"
					elif [ "${_table_name#cache*}" != "${_table_name}" ] || \
					     [ "${_table_name#data*}" != "${_table_name}" ] || \
					     [ "${_table_name#log*}" != "${_table_name}" ] || \
					     [ "${_table_name#tmp*}" != "${_table_name}" ]; then
						_mkfs_cmd="mkfs.f2fs -f -l ${_table_name}"
						_format_needed="true"
					elif [ "${_table_name#swap*}" != "${_table_name}" ]; then
						_mkfs_cmd="mkswap -L ${_table_name}"
						_format_needed="true"
					else
						continue
					fi
					;;
				esac

				echo "Attempting to setup partition ${_target_partition}"
				# In some cases of fsck, other values then 0 are acceptable,
				# as such we need to capture the return value or else set -u
				# will trigger as a failure and abort the script.
				if [ -n "${_fsck_cmd:-}" ] && [ -n "${_fsck_ret_ok:-}" ]; then
					_fsck_status="$(eval "${_fsck_cmd}" "${_target_partition}" 1> "/dev/null"; echo "${?}")"
					if [ "${_fsck_status}" -gt "${_fsck_ret_ok}" ] || \
					   ! eval "${_resize_cmd:?No reesize command set, bailing out}" "${_target_partition:?}"; then
						_format_needed="true"
					fi
				fi
				if [ "${_format_needed:-}" = "true" ]; then
					e_warn "Resize not possible, formatting instead."
					eval "${_mkfs_cmd:?No format command set, bailing out}" "${_target_partition:?}"
				fi
			fi
		done <<-EOF
			${_partition_table_file_data}
		EOF
	done <<-EOF
		${_partition_table_disk_data}
	EOF
}

partition_resize()
{
	_target_device="${1?Missing argument to function}"
	_partition_file="${2?Missing argument to function}"

	# !! Warning !! Changing partition layout can be fine or disastrous!
	# Always be careful when modifying partition tables!
	flock "${_target_device}" \
	      sfdisk --force --quiet "${_target_device}" < "${_partition_file}"
}

partition_table_sanity_check()
{
	_partition_file="${1?Missing argument to function}"

	echo "Validating disk sanity of ${_target_device}"
	if ! sfdisk --list --verify "${_target_device}"; then
	    e_warn "Current partition table is not healthy"
	fi

	# sfdisk returns size in blocks, * (1024 / 512) converts to sectors
	_target_disk_end="$(($(sfdisk --show-size --quiet "${_target_device}" 2> "/dev/null") * 2))"
	_partition_table_file_data="$(partition_table_extract "$(sort -n < "${_partition_file}")")"

	while IFS="," read -r _device _start _size _name; do
		if [ -z "${_device}" ] || \
		   is_comment "${_device}" || \
		   ! is_integer "${_start}" || \
		   ! is_integer "${_size}"; then
			continue
		fi

		if [ "${_name#boot*}" != "${_name}" ]; then
			boot_partition="${_device}"

			# The boot partition is special and should be a bit more spacious
			if [ "${_size}" -lt "$((PARTITION_EXT4_SIZE_MIN * 2))" ]; then
				e_err "Boot partition '${_device}' is too small (${_size} < $((PARTITION_EXT4_SIZE_MIN * 2)), cannot continue."
				exit 1
			fi
		elif [ "${_name#truststore*}" != "${_name}" ]; then
			if [ "${_size}" -lt "${PARTITION_EXT4_SIZE_MIN}" ]; then
				e_err "Truststore partition '${_device}' is too small (${_size} < ${PARTITION_EXT4_SIZE_MIN}), cannot continue."
				exit 1
			fi
		elif [ "${_name#cache*}" != "${_name}" ] || \
		     [ "${_name#data*}" != "${_name}" ] || \
		     [ "${_name#log*}" != "${_name}" ] || \
		     [ "${_name#tmp*}" != "${_name}" ]; then
			if [ "${_size}" -lt "${PARTITION_F2FS_SIZE_MIN}" ]; then
				e_err "Data partition '${_device}' is too small (${_size} < ${PARTITION_F2FS_SIZE_MIN}), cannot continue."
				exit 1
			fi
		elif [ "${_name#rom*}" != "${_name}" ]; then
			if [ "${_size}" -lt "${update_file_sectors}" ]; then
				e_err "ROM partition '${_device}' is too small (${_size} < ${update_file_sectors}), cannot continue."
				exit 1
			fi
		fi

		_partition_end="$((_start + _size))"
		if [ "${_partition_end}" -gt "${_target_disk_end}" ]; then
			e_err "Partition '${_device}' is beyond the size of the disk (${_partition_end} > ${_target_disk_end}), cannot continue."
			exit 1
		fi

		_identical_partitions=0
		while IFS="," read -r _verify_device _verify_start _verify_size _; do
			if [ -z "${_verify_device}" ] || \
			   is_comment "${_verify_device}" || \
			   ! is_integer "${_verify_start}" || \
			   ! is_integer "${_verify_size}"; then
				continue
			fi

			if [ "${_device}" = "${_verify_device}" ] || \
			   [ "${_start}" = "${_verify_start}" ]; then
				_identical_partitions="$((_identical_partitions + 1))"
				continue
			fi

			if [ "${_start}" -gt "${_verify_start}" ] && \
			   [ "${_start}" -lt "$((_verify_start + _verify_size))" ]; then
				e_err "Overlapping partitions detected (${_device}:${_start} ${_verify_device}:${_verify_start}-$((_verify_start + _verify_size))), cannot continue."
				exit 1
			fi
		done <<-EOF
			${_partition_table_file_data}
		EOF

		if [ "${_identical_partitions}" -ne 1 ]; then
			e_err "Invalid entries found for '${_device}'."
			exit 1
		fi
	done <<-EOF
		${_partition_table_file_data}
	EOF
}

needs_resize()
{
	_target_device="${1?Missing argument to function}"
	_partition_file="${2?Missing argument to function}"

	_partition_table_disk_data="$(partition_table_extract "$(sfdisk --quiet --dump "${_target_device}" | sort -n)")"
	_partition_table_file_data="$(partition_table_extract "$(sort -n < "${_partition_file}")")"

	if [ "${_partition_table_disk_data}" != "${_partition_table_file_data}" ]; then
		return 1
	fi

	return 0
}

verify_partition_file()
{
	_partition_file="${1?Missing argument to function}"

	if [ ! -f "${_partition_file}.sha512" ]; then
		e_err "Partition table file checksum not found '${_partition_file}.sha512'"
		exit 1
	fi

	(
		# change to update dir because path is hardcoded in the sha512 output.
		cd "$(dirname "${_partition_file}")"
		if ! sha512sum -c -w "${_partition_file}.sha512"; then
			e_err "Partition table file checksum error, cannot continue."
			exit 1
		fi
	)
}

prepare_disk()
{
	_target_device="${1:?}"
	_partition_table_file="${2:?}"

	if [ ! -f "${_partition_table_file}" ]; then
		echo "Partition table file not found '${_partition_table_file}', skipping"
		return
	fi

	verify_partition_file "${_partition_table_file}"

	partition_table_sanity_check "${_partition_table_file}"
	if [ -z "${boot_partition:-}" ] && \
	   [ -z "${_target_device}" ]; then
		e_err "No boot partition available, cannot continue."
		exit 1
	fi

	if ! needs_resize "${_target_device}" "${_partition_table_file}"; then
		partition_resize "${_target_device}" "${_partition_table_file}"
		partitions_sync "${_target_device}"
		partitions_format "${_target_device}" "${_partition_table_file}"
	fi
}

check_requirements()
{
	if [ ! -f "/proc/filesystems" ]; then
		echo "Cannot read /proc/filesystems, cannot continue"
		exit 1
	fi

	for _fs in ${REQUIRED_FILESYSTEMS}; do
		if grep -q "${_fs}" "/proc/filesystems"; then
			echo "${_fs} support: OK"
		else
			_fs_test_result=1
			echo "${_fs} support: ERROR"
		fi
	done

	if [ -n "${_fs_test_result:-}" ]; then
		echo "Self-test failed, missing filesystems."
		exit 1
	fi

	for _cmd in ${REQUIRED_COMMANDS}; do
		if ! _test_result="$(command -V "${_cmd}")"; then
			_test_result_fail="${_test_result_fail:-}${_test_result}\n"
		else
			_test_result_pass="${_test_result_pass:-}${_test_result}\n"
		fi
	done

	if [ -n "${_test_result_fail:-}" ]; then
		e_err "Self-test failed, missing dependencies."
		echo "======================================="
		echo "Passed tests:"
		# shellcheck disable=SC2059  # Interpret \n from variable
		printf "${_test_result_pass:-none\n}"
		echo "---------------------------------------"
		echo "Failed tests:"
		# shellcheck disable=SC2059  # Interpret \n from variable
		printf "${_test_result_fail:-none\n}"
		echo "======================================="
		exit 1
	fi
}

determine_partition_table_file()
{
	_partition_table_file_name=${1?Missing argument to function}
	partition_table_file="${_partition_table_file_name}"

	if [ -f "${SYSTEM_UPDATE_ETC_DIR}/${_partition_table_file_name}" ]; then
		partition_table_file="${SYSTEM_UPDATE_ETC_DIR}/${_partition_table_file_name}"

		return 0
	fi

	for _compatible in $(tr '\0' '
' < "${COMPATIBLES_PATH}" | \
	                     tac); do
		if [ -n "${_compatible:-}" ] && \
		   [ -f "${SYSTEM_UPDATE_ETC_DIR}/${_compatible}/${_partition_table_file_name}" ]; then
			partition_table_file="${SYSTEM_UPDATE_ETC_DIR}/${_compatible}/${_partition_table_file_name}"

			return 0
		fi
	done
}

main()
{
	while getopts ":b:d:fht:" options; do
		case "${options}" in
		b)
			echo "Boot bank is not relevant for '${0}', ignoring"
			;;
		d)
			target_device="${OPTARG}"
			;;
		f)
			force_failure="true"
			;;
		h)
			usage
			exit 0
			;;
		t)
			partition_table_file="$(readlink -f "${OPTARG}")"
			;;
		:)
			e_err "Option -${OPTARG} requires an argument."
			exit 1
			;;
		?)
			e_err "Invalid option: -${OPTARG}"
			exit 1
			;;
		esac
	done
	shift "$((OPTIND - 1))"

	if [ "${#}" -lt 1 ]; then
		e_err "Missing parameter"
		usage
		exit 1
	fi

	if [ "${#}" -gt 1 ]; then
		e_err "Too many parameters"
		usage
		exit 1
	fi

	update_file="${1}"

	if [ ! -f "${update_file:-}" ]; then
		e_err "Unable to read '${update_file}', cannot continue"
		usage
		exit 1
	fi

	if [ -z "${target_device:=${TARGET_DEVICE:-}}" ]; then
		e_err "Missing target storage device"
		usage
		exit 1
	fi

	check_requirements
	init

	if [ -f "${partition_table_file:-}" ]; then
		prepare_disk "${target_device}" "${partition_table_file}"
		exit 0
	fi

	for _device_postfix in "" ${DEVICE_PARTITION_POSTFIXES}; do
		_target_device="${target_device}${_device_postfix:-}"
		determine_partition_table_file "$(basename "${_target_device}").sfdisk"

		prepare_disk "${_target_device}" "${partition_table_file}"
	done
}

main "${@}"

exit 0
