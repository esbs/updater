#!/bin/sh
# SPDX-License-Identifier: GPL-2.0-or-later
#
# Copyright (C) 2018 EVBox B.V.
# Copyright (C) 2018 Olliver Schinagl <oliver@schinagl.nl>
# Copyright (C) 2020 Thibault Ferrante <thibault.ferrante@pm.me>

set -eu

# Support any invocation of/with shunit2
if [ "${0}" = "${0%%shunit2}" ]; then
	"$(command -v shunit2)" "${0}"
	return "${?}"
fi
src_dir="${1%%"${1##*'/'}"}"
COMMAND_UNDER_TEST="${COMMAND_UNDER_TEST:-${src_dir}/../scripts/${1##*/}}"
shift

FIXTURES="${FIXTURES:-${src_dir}/fixtures/}"
UINT32_MAX=0xffffffff

# shellcheck source=test/common.inc.sh
. "${src_dir}/common.inc.sh"

set +eu


BOOT_MTD_FLASH_SIZE="$((4096 * 25))"

get_config()
{
	_compatibles="${1?Missing argument to function}"

	for _compatible in $(tr '\000' '\n' < "${_compatibles}" | tac); do
		if [ -n "${_compatible:-}" ] && \
		   [ -f "${FIXTURES}/updater/${_compatible}/boot.config" ]; then
			# shellcheck source=/dev/null
			. "${FIXTURES}/updater/${_compatible}/boot.config"
		fi
		if [ -n "${_compatible:-}" ] && \
		   [ -f "${FIXTURES}/u-boot/${_compatible}/board.config" ]; then
			# shellcheck source=/dev/null
			. "${FIXTURES}/u-boot/${_compatible}/board.config"
		fi
	done
}

cmp_boot_env()
{
	_mount_point="${1?}"
	_fw_config="${2?}"
	_main_bank_expected="${3?}"
	_alt_bank_expected="${4?}"
	_write_count_expected="${5?}"

	assertEquals "Main bank variable should target '${_main_bank_expected}'" \
	              "${_main_bank_expected}" \
	              "$(chroot "${_mount_point}" fw_printenv \
	                                                       --lock "/run" \
	                                                       --config "${_fw_config}" \
	                                                       --noheader \
	                                                       "${ENVIRONMENT_BANK_VARIABLE_NAME}")"

	assertEquals "Alternative bank variable should target '${_alt_bank_expected}'" \
	              "${_alt_bank_expected}" \
	              "$(chroot "${_mount_point}" fw_printenv \
	                                                       --lock "/run" \
	                                                       --config "${_fw_config}" \
	                                                       --noheader \
	                                                       "${ENVIRONMENT_BANK_ALT_VARIABLE_NAME}")"
	assertEquals "Write count should be correctly updated" \
	              "${_write_count_expected}" \
	              "$(chroot "${_mount_point}" fw_printenv \
	                                                       --lock "/run" \
	                                                       --config "${_fw_config}" \
	                                                       --noheader \
	                                                       "${ENVIRONMENT_WRITE_COUNT_NAME}")"
}

create_ro_blockdevice()
{
	_mount_point="${1?}"
	_target_mount="${2?}"
	_size="${3:-${BOOT_MTD_FLASH_SIZE}}"
	_test_mtd_target_device_img="$(mktemp -p "${SHUNIT_TMPDIR}" "test_mtd_target_device_img.XXXXXX")"
	fallocate -l "${_size}" "${_test_mtd_target_device_img}"
	_test_mtd_target_device="$(losetup --find --partscan -r --show "${_test_mtd_target_device_img}")"
	ln -ns "${_test_mtd_target_device}" "${_mount_point}/${_target_mount}"
	loop_devices="${loop_devices:-} ${_test_mtd_target_device_img}"
}

detach_loop_device()
{
	_loop_device="${1?}"
	_target_devices="$(losetup --associated "${_loop_device}" --noheadings --output 'NAME' --raw)"

	for _target_device in ${_target_devices}; do
		if ! losetup --detach "${_target_device}"; then
			fail "Unable to detach loop device '${_target_device}'"
		fi
	done
}

create_initialized_ro_blockdevice()
{
	_mount_point="${1?}"
	_target_mount="${2?}"
	_target_conf="${3?}"
	_size="${4:-${BOOT_MTD_FLASH_SIZE}}"
	_test_mtd_target_device_img="$(mktemp -p "${SHUNIT_TMPDIR}" "test_mtd_target_device_img.XXXXXX")"
	fallocate -l "${_size}" "${_test_mtd_target_device_img}"

	echo "${_target_mount} 0x0000 0x8000 0x8000" > "${_mount_point}/${_target_conf}"

	_test_mtd_target_device="$(losetup --find --partscan --show "${_test_mtd_target_device_img}")"
	ln -ns "${_test_mtd_target_device}" "${_mount_point}/${_target_mount}"
	set_boot_env "${_mount_point}" "${_target_conf}" "a" "b" "0x2"
	detach_loop_device "${_test_mtd_target_device_img}"
	unlink "${_mount_point}/${_target_mount}"

	_test_mtd_target_device="$(losetup --find --partscan -r --show "${_test_mtd_target_device_img}")"
	ln -ns "${_test_mtd_target_device}" "${_mount_point}/${_target_mount}"
	loop_devices="${loop_devices:-} ${_test_mtd_target_device_img}"
}

init_boot_env()
{
	_mount_point="${1?}"
	_target_conf="${2?}"
	_target_file="${3:-/boot/u-boot.env}"
	echo "${_target_file} 0x0000 0x8000" > "${_mount_point}/${_target_conf}"
	mkdir -p "${test_update_mountpoint}/${_target_file%/*}"
	dd if="/dev/zero" of="${_mount_point}/${_target_file}" bs=1024 count=32 > "/dev/null" 2>&1
	chroot "${_mount_point}" fw_setenv  --lock "/run" -c "${_target_conf}" env_version 0
}

set_boot_env()
{
	_mount_point="${1?}"
	_fw_config="${2?}"
	_main_bank_value="${3?}"
	_alt_bank_value="${4?}"
	_write_count="${5?}"
	chroot "${_mount_point}" fw_setenv \
	                               --lock "/run" \
	                               --config "${_fw_config}" \
	                               "${ENVIRONMENT_BANK_VARIABLE_NAME}" "${_main_bank_value}"

	chroot "${_mount_point}" fw_setenv \
	                               --lock "/run" \
	                               --config "${_fw_config}" \
	                               "${ENVIRONMENT_BANK_ALT_VARIABLE_NAME}" "${_alt_bank_value}"

	chroot "${_mount_point}" fw_setenv \
	                               --lock "/run" \
	                               --config "${_fw_config}" \
	                               "${ENVIRONMENT_WRITE_COUNT_NAME}" "${_write_count}"
}

set_bootargs()
{
	_bank_targeted="${1?}"
	if [ "${_bank_targeted}" = "a" ]; then
		_root_value="${test_dummy_storage_device}p2"
	else
		_root_value="${test_dummy_storage_device}p5"
	fi

	echo "console=ttyS0,115200n8 root=${_root_value} rw zswap.zpool=z3fold" > "${bootargs_binding}"
}

oneTimeSetUp()
{
	echo "Setting up '${COMMAND_UNDER_TEST##*/}' test env"

	bootargs_binding="$(mktemp -p "${SHUNIT_TMPDIR}" "bootargs_binding.XXXXXX")"
	swupdate_src="$(mktemp -p "${SHUNIT_TMPDIR}" "swupdate_src.XXXXXX")"
	cp "${SOFTWARE_UPDATE_FILE}" "${swupdate_src}"

	test_squashfs_root_dir="$(mktemp -d -p "${SHUNIT_TMPDIR}" "test_squashfs_root_dir.XXXXXX")"
	squashfs_inject_path "${swupdate_src}" \
	                     "${FIXTURES}/updater/" \
	                     "/usr/share"
	squashfs_inject_path "${swupdate_src}" \
	                     "${FIXTURES}/u-boot/" \
	                     "/usr/share"
	squashfs_inject_path "${swupdate_src}" \
	                     "${COMMAND_UNDER_TEST}" \
	                     "/usr/sbin"
	COMMAND_UNDER_TEST="/usr/sbin/$(basename "${COMMAND_UNDER_TEST}")"

	echo
	echo "================================================================================"
}

oneTimeTearDown()
{
	echo "Tearing down '${COMMAND_UNDER_TEST##*/}' test env"

	if [ -d "${test_squashfs_root_dir:-}" ]; then
		rm -rf "${test_squashfs_root_dir?}"
	fi

	if [ -f "${swupdate_src}" ]; then
		unlink "${swupdate_src}"
	fi
}

setUp()
{
	echo

	unset ENVIRONMENT_BANK_ALT_VARIABLE_NAME
	unset ENVIRONMENT_BANK_VARIABLE_NAME
	unset ENVIRONMENT_WRITE_COUNT_NAME
	unset PLATFORM_TARGET_DEVICE
	unset UBOOT_ENV_CONFIG
	unset UBOOT_ENV_COPY_CONFIG

	test_swupdate_file="$(mktemp -p "${SHUNIT_TMPDIR}" "test_swupdate_file.XXXXXX")"
	cp "${swupdate_src}" "${test_swupdate_file}"


	test_target_device_img="$(mktemp -p "${SHUNIT_TMPDIR}" "test_target_device_img.XXXXXX")"
	create_dummy_storage_device "${test_target_device_img}"

	flock "${test_dummy_storage_device}" \
	      sfdisk --quiet "${test_dummy_storage_device}" < "${FIXTURES}/emmc_partition_table_template.sfdisk"

	# Do not store the update mount in the SHUNIT tmpdir, as unexpected failures
	# will trigger an rm -rf of the SHUNIT tmpdir, including the mounted /dev,
	# causing the host system to fail terribly. As neither tearDown nor
	# oneTimeTeardown is called; this will leave cruft in case of failures.
	# Until shunit2 implements a tearDown hook this cannot be avoided.
	test_update_mountpoint="$(mktemp -d -p "${TMPDIR:-/tmp}" "test_update_mountpoint.XXXXXX")"

	mount_chroot_test_env "${test_swupdate_file}" "${test_update_mountpoint}"
	install -D -d -m 655 \
	        "${test_update_mountpoint}/sys/firmware/devicetree/base"
	echo "" > "${bootargs_binding}"
	mount --bind "${bootargs_binding}" "${test_update_mountpoint}/proc/cmdline"
	test_log_output="$(mktemp -p "${SHUNIT_TMPDIR}" "test_log_output.XXXXXX")"
}

tearDown()
{
	if [ -f "${test_log_output:-}" ]; then
		unlink "${test_log_output}"
	fi

	umount "${test_update_mountpoint}/proc/cmdline"

	umount_chroot_test_env "${test_swupdate_file}" "${test_update_mountpoint}"

	destroy_dummy_storage_device "${test_target_device_img}"
	if [ -n "${loop_devices:-}" ]; then
		for _loop_device in ${loop_devices}; do
			detach_loop_device "${_loop_device}"
			unlink "${_loop_device}"
		done
		loop_devices=""
	fi

	if [ -f "${test_swupdate_file:-}" ]; then
		unlink "${test_swupdate_file}"
	fi

	if mountpoint -q "${test_update_mountpoint:-}"; then
		umount "${test_update_mountpoint}"
	fi

	if [ -d "${test_update_mountpoint}" ]; then
		rmdir "${test_update_mountpoint}"
	fi

	echo "--------------------------------------------------------------------------------"
}

testGettingBootTargetDevice()
{
	printf "test,dual-boot-env\000" > "${test_update_mountpoint}/sys/firmware/devicetree/base/compatible"
	get_config "${test_update_mountpoint}/sys/firmware/devicetree/base/compatible"

	chroot "${test_update_mountpoint}" "${COMMAND_UNDER_TEST}" -d > "${test_log_output}"
	assertTrue "Target device should be found" "[ ${?} -eq 0 ]"

	assertEquals "Correct plaftorm should be used" \
	             "${UPDATE_TARGET_DEVICE}" \
	             "$(cat "${test_log_output}")"
}

testSwapTargetBankWithDualBoot()
{
	printf "test,dual-boot-env\000" > "${test_update_mountpoint}/sys/firmware/devicetree/base/compatible"
	get_config "${test_update_mountpoint}/sys/firmware/devicetree/base/compatible"

	init_boot_env "${test_update_mountpoint}" "${UBOOT_ENV_CONFIG}" "/boot/u-boot.env"

	chroot "${test_update_mountpoint}" "${COMMAND_UNDER_TEST}" -s
	assertTrue "Update target bank should succeed with default values" "[ ${?} -eq 0 ]"
	cmp_boot_env "${test_update_mountpoint}" "${UBOOT_ENV_CONFIG}" "a" "b" "0x1"

	chroot "${test_update_mountpoint}" "${COMMAND_UNDER_TEST}" -s
	assertTrue "Update target bank should succeed after the first flash" "[ ${?} -eq 0 ]"
	cmp_boot_env "${test_update_mountpoint}" "${UBOOT_ENV_CONFIG}" "b" "a" "0x2"

	chroot "${test_update_mountpoint}" "${COMMAND_UNDER_TEST}" -s
	assertTrue "Update target bank should succeed after the second flash" "[ ${?} -eq 0 ]"
	cmp_boot_env "${test_update_mountpoint}" "${UBOOT_ENV_CONFIG}" "a" "b" "0x3"
}

testSwapTargetBankWithDualBootWithTens()
{
	printf "test,dual-boot-env\000" > "${test_update_mountpoint}/sys/firmware/devicetree/base/compatible"
	get_config "${test_update_mountpoint}/sys/firmware/devicetree/base/compatible"

	init_boot_env "${test_update_mountpoint}" "${UBOOT_ENV_CONFIG}" "/boot/u-boot.env"
	init_boot_env "${test_update_mountpoint}" "${UBOOT_ENV_COPY_CONFIG}" "/boot/u-boot.env2"

	set_boot_env "${test_update_mountpoint}" "${UBOOT_ENV_CONFIG}" "a" "b" "0x9"
	set_boot_env "${test_update_mountpoint}" "${UBOOT_ENV_COPY_CONFIG}" "a" "b" "0x9"

	chroot "${test_update_mountpoint}" "${COMMAND_UNDER_TEST}" -s
	assertTrue "Update target bank should succeed with default values" "[ ${?} -eq 0 ]"
	cmp_boot_env "${test_update_mountpoint}" "${UBOOT_ENV_CONFIG}" "b" "a" "0xa"
	cmp_boot_env "${test_update_mountpoint}" "${UBOOT_ENV_COPY_CONFIG}" "b" "a" "0xa"

	set_boot_env "${test_update_mountpoint}" "${UBOOT_ENV_CONFIG}" "a" "b" "0xf"
	set_boot_env "${test_update_mountpoint}" "${UBOOT_ENV_COPY_CONFIG}" "a" "b" "0xf"

	chroot "${test_update_mountpoint}" "${COMMAND_UNDER_TEST}" -s
	assertTrue "Update target bank should succeed after the first flash" "[ ${?} -eq 0 ]"
	cmp_boot_env "${test_update_mountpoint}" "${UBOOT_ENV_CONFIG}" "b" "a" "0x10"
	cmp_boot_env "${test_update_mountpoint}" "${UBOOT_ENV_COPY_CONFIG}" "b" "a" "0x10"

	set_boot_env "${test_update_mountpoint}" "${UBOOT_ENV_CONFIG}" "a" "b" "0xff"
	set_boot_env "${test_update_mountpoint}" "${UBOOT_ENV_COPY_CONFIG}" "a" "b" "0xff"

	chroot "${test_update_mountpoint}" "${COMMAND_UNDER_TEST}" -s
	assertTrue "Update target bank should succeed after the second flash" "[ ${?} -eq 0 ]"
	cmp_boot_env "${test_update_mountpoint}" "${UBOOT_ENV_CONFIG}" "b" "a" "0x100"
	cmp_boot_env "${test_update_mountpoint}" "${UBOOT_ENV_COPY_CONFIG}" "b" "a" "0x100"

	set_boot_env "${test_update_mountpoint}" "${UBOOT_ENV_CONFIG}" "a" "b" "$((UINT32_MAX))"
	set_boot_env "${test_update_mountpoint}" "${UBOOT_ENV_COPY_CONFIG}" "a" "b" "$((UINT32_MAX))"

	chroot "${test_update_mountpoint}" "${COMMAND_UNDER_TEST}" -s
	assertTrue "Update target bank should succeed after the second flash" "[ ${?} -eq 0 ]"
	cmp_boot_env "${test_update_mountpoint}" "${UBOOT_ENV_CONFIG}" "b" "a" "0x1"
	cmp_boot_env "${test_update_mountpoint}" "${UBOOT_ENV_COPY_CONFIG}" "b" "a" "0x1"

	set_boot_env "${test_update_mountpoint}" "${UBOOT_ENV_CONFIG}" "a" "b" "${UINT32_MAX}"
	set_boot_env "${test_update_mountpoint}" "${UBOOT_ENV_COPY_CONFIG}" "a" "b" "${UINT32_MAX}"

	chroot "${test_update_mountpoint}" "${COMMAND_UNDER_TEST}" -s
	assertTrue "Update target bank should succeed after the second flash" "[ ${?} -eq 0 ]"
	cmp_boot_env "${test_update_mountpoint}" "${UBOOT_ENV_CONFIG}" "b" "a" "0x1"
	cmp_boot_env "${test_update_mountpoint}" "${UBOOT_ENV_COPY_CONFIG}" "b" "a" "0x1"
}


testUpdateTargetBankWithFailingWrites()
{
	printf "test,dual-boot-env\000" > "${test_update_mountpoint}/sys/firmware/devicetree/base/compatible"
	get_config "${test_update_mountpoint}/sys/firmware/devicetree/base/compatible"

	init_boot_env "${test_update_mountpoint}" "${UBOOT_ENV_CONFIG}" "/boot/u-boot.env"
	set_boot_env "${test_update_mountpoint}" "${UBOOT_ENV_CONFIG}" "a" "b" "0x2"

	command_failure "${test_update_mountpoint}/usr/bin/fw_setenv"
	chroot "${test_update_mountpoint}" "${COMMAND_UNDER_TEST}" -s
	assertFalse "Update target bank should fail if fw_setenv fail" "[ ${?} -eq 0 ]"
}

testUpdateTargetBankWithTwoEnvironmentUpdated()
{
	printf "test,dual-boot-env\000" > "${test_update_mountpoint}/sys/firmware/devicetree/base/compatible"
	get_config "${test_update_mountpoint}/sys/firmware/devicetree/base/compatible"

	init_boot_env "${test_update_mountpoint}" "${UBOOT_ENV_CONFIG}" "/boot/u-boot.env1"
	init_boot_env "${test_update_mountpoint}" "${UBOOT_ENV_COPY_CONFIG}" "/boot/u-boot.env2"

	chroot "${test_update_mountpoint}" "${COMMAND_UNDER_TEST}" -s
	assertTrue "Update target bank should succeed with default values" "[ ${?} -eq 0 ]"
	for _env in ${UBOOT_ENV_CONFIG} ${UBOOT_ENV_COPY_CONFIG:-}; do
		cmp_boot_env "${test_update_mountpoint}" "${_env}" "a" "b" "0x1"
	done

	chroot "${test_update_mountpoint}" "${COMMAND_UNDER_TEST}" -s
	assertTrue "Update target bank should succeed after the first flash" "[ ${?} -eq 0 ]"
	for _env in ${UBOOT_ENV_CONFIG} ${UBOOT_ENV_COPY_CONFIG:-}; do
		cmp_boot_env "${test_update_mountpoint}" "${_env}" "b" "a" "0x2"
	done

	chroot "${test_update_mountpoint}" "${COMMAND_UNDER_TEST}" -s
	assertTrue "Update target bank should succeed after the second flash" "[ ${?} -eq 0 ]"
	for _env in ${UBOOT_ENV_CONFIG} ${UBOOT_ENV_COPY_CONFIG:-}; do
		cmp_boot_env "${test_update_mountpoint}" "${_env}" "a" "b" "0x3"
	done
}

testUpdateTargetBankWithOneBrokenEnvironment()
{
	printf "test,dual-boot-env\000" > "${test_update_mountpoint}/sys/firmware/devicetree/base/compatible"
	get_config "${test_update_mountpoint}/sys/firmware/devicetree/base/compatible"

	init_boot_env "${test_update_mountpoint}" "${UBOOT_ENV_CONFIG}"
	echo "/dev/null 0x0000 0x8000" > "${test_update_mountpoint}/${UBOOT_ENV_COPY_CONFIG}"

	chroot "${test_update_mountpoint}" "${COMMAND_UNDER_TEST}" -s
	assertTrue "Update target bank should succeed with default values" "[ ${?} -eq 0 ]"
	cmp_boot_env "${test_update_mountpoint}" "${UBOOT_ENV_CONFIG}" "a" "b" "0x1"

	chroot "${test_update_mountpoint}" "${COMMAND_UNDER_TEST}" -s
	assertTrue "Update target bank should succeed after the first flash" "[ ${?} -eq 0 ]"
	cmp_boot_env "${test_update_mountpoint}" "${UBOOT_ENV_CONFIG}" "b" "a" "0x2"

	chroot "${test_update_mountpoint}" "${COMMAND_UNDER_TEST}" -s
	assertTrue "Update target bank should succeed after the second flash" "[ ${?} -eq 0 ]"
	cmp_boot_env "${test_update_mountpoint}" "${UBOOT_ENV_CONFIG}" "a" "b" "0x3"
}

testGettingTargetBankWithDualBoot()
{
	printf "test,dual-boot-env\000" > "${test_update_mountpoint}/sys/firmware/devicetree/base/compatible"
	get_config "${test_update_mountpoint}/sys/firmware/devicetree/base/compatible"

	init_boot_env "${test_update_mountpoint}" "${UBOOT_ENV_CONFIG}"

	chroot "${test_update_mountpoint}" "${COMMAND_UNDER_TEST}" -b > "${test_log_output}"
	assertTrue "Default bank device should be found without a boot env" "[ ${?} -eq 0 ]"
	assertEquals "Default bank should be used" \
	             "${DEFAULT_BANK}" \
	             "$(cat "${test_log_output}")"

	set_boot_env "${test_update_mountpoint}" "${UBOOT_ENV_CONFIG}" "a" "b" "0x1"
	chroot "${test_update_mountpoint}" "${COMMAND_UNDER_TEST}" -b > "${test_log_output}"
	assertTrue "Target bank should be found" "[ ${?} -eq 0 ]"
	assertEquals "Correct bank should be found" \
	             "b" \
	             "$(cat "${test_log_output}")"

	set_boot_env "${test_update_mountpoint}" "${UBOOT_ENV_CONFIG}" "b" "a" "0x2"
	chroot "${test_update_mountpoint}" "${COMMAND_UNDER_TEST}" -b > "${test_log_output}"
	assertTrue "Target bank should be found" "[ ${?} -eq 0 ]"
	assertEquals "Correct bank should be found" \
	             "a" \
	             "$(cat "${test_log_output}")"
}

testInconsistBootargsEnvironmentGetTargetBank()
{
	printf "test,dual-boot-env\0" > "${test_update_mountpoint}/sys/firmware/devicetree/base/compatible"
	get_config "${test_update_mountpoint}/sys/firmware/devicetree/base/compatible"

	init_boot_env "${test_update_mountpoint}" "${UBOOT_ENV_CONFIG}" "/boot/u-boot.env1"
	init_boot_env "${test_update_mountpoint}" "${UBOOT_ENV_COPY_CONFIG}" "/boot/u-boot.env2"
	set_boot_env "${test_update_mountpoint}" "${UBOOT_ENV_CONFIG}" "b" "a" "0x2"
	set_boot_env "${test_update_mountpoint}" "${UBOOT_ENV_COPY_CONFIG}" "b" "a" "0x2"
	set_bootargs "a"

	chroot "${test_update_mountpoint}" "${COMMAND_UNDER_TEST}" -b > "${test_log_output}"
	assertTrue "Bank should be swapped accordingly to the bootargs" "[ ${?} -eq 0 ]"
	assertEquals "Correct bank should be used" \
	             "b" \
	             "$(cat "${test_log_output}")"

	cmp_boot_env "${test_update_mountpoint}" "${UBOOT_ENV_CONFIG}" "a" "b" "0x3"
	cmp_boot_env "${test_update_mountpoint}" "${UBOOT_ENV_COPY_CONFIG}" "a" "b" "0x3"
}

testInconsistBootargsEnvironmentSwapping()
{
	printf "test,dual-boot-env\000" > "${test_update_mountpoint}/sys/firmware/devicetree/base/compatible"
	get_config "${test_update_mountpoint}/sys/firmware/devicetree/base/compatible"

	init_boot_env "${test_update_mountpoint}" "${UBOOT_ENV_CONFIG}" "/boot/u-boot.env1"
	init_boot_env "${test_update_mountpoint}" "${UBOOT_ENV_COPY_CONFIG}" "/boot/u-boot.env2"
	set_boot_env "${test_update_mountpoint}" "${UBOOT_ENV_CONFIG}" "b" "a" "0x2"
	set_boot_env "${test_update_mountpoint}" "${UBOOT_ENV_COPY_CONFIG}" "b" "a" "0x2"
	set_bootargs "a"

	chroot "${test_update_mountpoint}" "${COMMAND_UNDER_TEST}" -s > "${test_log_output}"
	assertTrue "Bank should be swapped accordingly to the bootargs" "[ ${?} -eq 0 ]"

	cmp_boot_env "${test_update_mountpoint}" "${UBOOT_ENV_CONFIG}" "b" "a" "0x4"
	cmp_boot_env "${test_update_mountpoint}" "${UBOOT_ENV_COPY_CONFIG}" "b" "a" "0x4"

	set_boot_env "${test_update_mountpoint}" "${UBOOT_ENV_CONFIG}" "b" "a" "${UINT32_MAX}"
	set_boot_env "${test_update_mountpoint}" "${UBOOT_ENV_COPY_CONFIG}" "b" "a" "${UINT32_MAX}"
	set_bootargs "a"

	chroot "${test_update_mountpoint}" "${COMMAND_UNDER_TEST}" -s > "${test_log_output}"
	assertTrue "Bank should be swapped accordingly to the bootargs" "[ ${?} -eq 0 ]"

	cmp_boot_env "${test_update_mountpoint}" "${UBOOT_ENV_CONFIG}" "b" "a" "0x2"
	cmp_boot_env "${test_update_mountpoint}" "${UBOOT_ENV_COPY_CONFIG}" "b" "a" "0x2"
}

testInconsistBootargsEnvironmentGetTargetBankWithBrokenEnvironment()
{
	printf "test,dual-boot-env\000" > "${test_update_mountpoint}/sys/firmware/devicetree/base/compatible"
	get_config "${test_update_mountpoint}/sys/firmware/devicetree/base/compatible"


	create_initialized_ro_blockdevice "${test_update_mountpoint}" "/dev/mtd2" "${UBOOT_ENV_CONFIG}"
	cmp_boot_env "${test_update_mountpoint}" "${UBOOT_ENV_CONFIG}" "a" "b" "0x2"
	create_initialized_ro_blockdevice "${test_update_mountpoint}" "/dev/mtd3" "${UBOOT_ENV_COPY_CONFIG}"
	cmp_boot_env "${test_update_mountpoint}" "${UBOOT_ENV_COPY_CONFIG}" "a" "b" "0x2"
	set_bootargs "b"

	chroot "${test_update_mountpoint}" "${COMMAND_UNDER_TEST}" -b > "${test_log_output}"
	assertFalse "As environment are inconsistent with bootargs, and that the environment can't be repaired, \
it's not possible to get the target bank" "[ ${?} -eq 0 ]"

	chroot "${test_update_mountpoint}" "${COMMAND_UNDER_TEST}" -s > "${test_log_output}"
	assertFalse "As environment are inconsistent with bootargs, and that the environment can't be repaired, \
it's not possible to swap banks" "[ ${?} -eq 0 ]"
}

testNoBootEnvSetupShouldUseDefault()
{
	printf "test,dual-boot-env\000" > "${test_update_mountpoint}/sys/firmware/devicetree/base/compatible"
	get_config "${test_update_mountpoint}/sys/firmware/devicetree/base/compatible"

	chroot "${test_update_mountpoint}" "${COMMAND_UNDER_TEST}" -b > "${test_log_output}"
	assertTrue "Default bank device should be found without a boot env" "[ ${?} -eq 0 ]"

	assertEquals "Default bank should be used" \
	             "${DEFAULT_BANK}" \
	             "$(cat "${test_log_output}")"
}

testNoCompatible()
{
	chroot "${test_update_mountpoint}" "${COMMAND_UNDER_TEST}" -d > "${test_log_output}"
	assertFalse "Missing compatible should not be accepted." "[ ${?} -eq 0 ]"

	chroot "${test_update_mountpoint}" "${COMMAND_UNDER_TEST}" -b
	assertFalse "Missing compatible should not be accepted." "[ ${?} -eq 0 ]"

	chroot "${test_update_mountpoint}" "${COMMAND_UNDER_TEST}" -s
	assertFalse "Missing compatible should not be accepted." "[ ${?} -eq 0 ]"
}

testEmptyCompatible()
{
	printf "\000" > "${test_update_mountpoint}/sys/firmware/devicetree/base/compatible"

	chroot "${test_update_mountpoint}" "${COMMAND_UNDER_TEST}" -d
	assertFalse "Empty compatible should not be accepted." "[ ${?} -eq 0 ]"

	chroot "${test_update_mountpoint}" "${COMMAND_UNDER_TEST}" -b
	assertFalse "Empty compatible should not be accepted." "[ ${?} -eq 0 ]"

	chroot "${test_update_mountpoint}" "${COMMAND_UNDER_TEST}" -s
	assertFalse "Empty compatible should not be accepted." "[ ${?} -eq 0 ]"
}

testInvalidCompatible()
{
	printf "test,no-valid-board\000" > "${test_update_mountpoint}/sys/firmware/devicetree/base/compatible"

	chroot "${test_update_mountpoint}" "${COMMAND_UNDER_TEST}" -d
	assertFalse "Invalid compatible should not be accepted." "[ ${?} -eq 0 ]"

	chroot "${test_update_mountpoint}" "${COMMAND_UNDER_TEST}" -b
	assertFalse "Invalid compatible should not be accepted." "[ ${?} -eq 0 ]"

	chroot "${test_update_mountpoint}" "${COMMAND_UNDER_TEST}" -s
	assertFalse "Invalid compatible should not be accepted." "[ ${?} -eq 0 ]"
}

testUpdateTargetBankWithOnlyOneInitialized()
{
	printf "test,dual-boot-env\0" > "${test_update_mountpoint}/sys/firmware/devicetree/base/compatible"
	get_config "${test_update_mountpoint}/sys/firmware/devicetree/base/compatible"

	init_boot_env "${test_update_mountpoint}" "${UBOOT_ENV_CONFIG}" "/boot/u-boot.env1"
	init_boot_env "${test_update_mountpoint}" "${UBOOT_ENV_COPY_CONFIG}" "/boot/u-boot.env2"

	set_boot_env "${test_update_mountpoint}" "${UBOOT_ENV_CONFIG}" "a" "b" "0x10"

	chroot "${test_update_mountpoint}" "${COMMAND_UNDER_TEST}" -b > "${test_log_output}"
	assertTrue "The environment used should be the initialized one" "[ ${?} -eq 0 ]"

	assertEquals "Initialized environment should be used" \
	             "b" \
	             "$(cat "${test_log_output}")"

	cmp_boot_env "${test_update_mountpoint}" "${UBOOT_ENV_CONFIG}" "a" "b" "0x10"
	cmp_boot_env "${test_update_mountpoint}" "${UBOOT_ENV_COPY_CONFIG}" "a" "b" "0x10"

}

testUpdateTargetBankWithDifferentCounter()
{
	printf "test,dual-boot-env\0" > "${test_update_mountpoint}/sys/firmware/devicetree/base/compatible"
	get_config "${test_update_mountpoint}/sys/firmware/devicetree/base/compatible"

	init_boot_env "${test_update_mountpoint}" "${UBOOT_ENV_CONFIG}" "/boot/u-boot.env1"
	init_boot_env "${test_update_mountpoint}" "${UBOOT_ENV_COPY_CONFIG}" "/boot/u-boot.env2"

	set_boot_env "${test_update_mountpoint}" "${UBOOT_ENV_CONFIG}" "a" "b" "0x1"
	set_boot_env "${test_update_mountpoint}" "${UBOOT_ENV_COPY_CONFIG}" "b" "a" "0x2"

	chroot "${test_update_mountpoint}" "${COMMAND_UNDER_TEST}" -b > "${test_log_output}"
	assertTrue "The environment used should be the one with the highest count" "[ ${?} -eq 0 ]"

	assertEquals "Environment with the highest counter should be used" \
	             "a" \
	             "$(cat "${test_log_output}")"

	cmp_boot_env "${test_update_mountpoint}" "${UBOOT_ENV_CONFIG}" "b" "a" "0x2"
	cmp_boot_env "${test_update_mountpoint}" "${UBOOT_ENV_COPY_CONFIG}" "b" "a" "0x2"

	set_boot_env "${test_update_mountpoint}" "${UBOOT_ENV_CONFIG}" "a" "b" "0x2"
	set_boot_env "${test_update_mountpoint}" "${UBOOT_ENV_COPY_CONFIG}" "b" "a" "0x1"

	chroot "${test_update_mountpoint}" "${COMMAND_UNDER_TEST}" -b > "${test_log_output}"
	assertTrue "The environment used should be the one with the highest count" "[ ${?} -eq 0 ]"

	assertEquals "Environment with the highest counter should be used" \
	             "b" \
	             "$(cat "${test_log_output}")"

	chroot "${test_update_mountpoint}" "${COMMAND_UNDER_TEST}" -s
	assertTrue "Update target bank should succeed even when one environment is outdated" "[ ${?} -eq 0 ]"
	for _env in ${UBOOT_ENV_CONFIG} ${UBOOT_ENV_COPY_CONFIG:-}; do
		cmp_boot_env "${test_update_mountpoint}" "${_env}" "b" "a" "0x3"
	done

	set_boot_env "${test_update_mountpoint}" "${UBOOT_ENV_CONFIG}" "a" "b" "0x2"
	set_boot_env "${test_update_mountpoint}" "${UBOOT_ENV_COPY_CONFIG}" "b" "a" "0x2"

	chroot "${test_update_mountpoint}" "${COMMAND_UNDER_TEST}" -b > "${test_log_output}"
	assertTrue "The environment used should be the one the first with the highest count" "[ ${?} -eq 0 ]"

	assertEquals "The first environment with the highest counter should be used" \
	             "b" \
	             "$(cat "${test_log_output}")"
}

testUpdateTargetBankWithOneUninitializedRO()
{
	printf "test,dual-boot-env\0" > "${test_update_mountpoint}/sys/firmware/devicetree/base/compatible"
	get_config "${test_update_mountpoint}/sys/firmware/devicetree/base/compatible"

	init_boot_env "${test_update_mountpoint}" "${UBOOT_ENV_CONFIG}"

	create_ro_blockdevice "${test_update_mountpoint}" "/dev/mtd4"
	echo "/dev/mtd4 0x0000 0x8000 0x8000" > "${test_update_mountpoint}/${UBOOT_ENV_COPY_CONFIG}"

	set_boot_env "${test_update_mountpoint}" "${UBOOT_ENV_CONFIG}" "a" "b" "0x10"

	chroot "${test_update_mountpoint}" "${COMMAND_UNDER_TEST}" -b > "${test_log_output}"
	assertTrue "The environment used should be the initialized one" "[ ${?} -eq 0 ]"

	assertEquals "Initialized environment should be used" \
	             "b" \
	             "$(cat "${test_log_output}")"

	cmp_boot_env "${test_update_mountpoint}" "${UBOOT_ENV_CONFIG}" "a" "b" "0x10"

	chroot "${test_update_mountpoint}" "${COMMAND_UNDER_TEST}" -s
	assertTrue "Update target bank should succeed even when one environment is outdated" "[ ${?} -eq 0 ]"

	cmp_boot_env "${test_update_mountpoint}" "${UBOOT_ENV_CONFIG}" "b" "a" "0x11"
}

testUpdateTargetBankWithOneInitializedRO()
{
	printf "test,dual-boot-env\0" > "${test_update_mountpoint}/sys/firmware/devicetree/base/compatible"
	get_config "${test_update_mountpoint}/sys/firmware/devicetree/base/compatible"

	init_boot_env "${test_update_mountpoint}" "${UBOOT_ENV_CONFIG}"
	set_boot_env "${test_update_mountpoint}" "${UBOOT_ENV_CONFIG}" "a" "b" "0x10"

	create_initialized_ro_blockdevice "${test_update_mountpoint}" "/dev/mtd2" "${UBOOT_ENV_COPY_CONFIG}"
	cmp_boot_env "${test_update_mountpoint}" "${UBOOT_ENV_COPY_CONFIG}" "a" "b" "0x2"


	chroot "${test_update_mountpoint}" "${COMMAND_UNDER_TEST}" -b > "${test_log_output}"
	assertTrue "If two environment are not consistent and can't be fixed, it's a critical failure" "[ ${?} -eq 1 ]"

	cmp_boot_env "${test_update_mountpoint}" "${UBOOT_ENV_CONFIG}" "a" "b" "0x10"
	cmp_boot_env "${test_update_mountpoint}" "${UBOOT_ENV_COPY_CONFIG}" "a" "b" "0x2"

	chroot "${test_update_mountpoint}" "${COMMAND_UNDER_TEST}" -s
	assertTrue "If two environment are not consistent and can't be fixed, it's a critical failure" "[ ${?} -eq 1 ]"

	cmp_boot_env "${test_update_mountpoint}" "${UBOOT_ENV_CONFIG}" "a" "b" "0x10"
	cmp_boot_env "${test_update_mountpoint}" "${UBOOT_ENV_COPY_CONFIG}" "a" "b" "0x2"
}

testUpdateTargetBankWithMainOneInitializedRO()
{
	printf "test,dual-boot-env\0" > "${test_update_mountpoint}/sys/firmware/devicetree/base/compatible"
	get_config "${test_update_mountpoint}/sys/firmware/devicetree/base/compatible"

	init_boot_env "${test_update_mountpoint}" "${UBOOT_ENV_COPY_CONFIG}"
	set_boot_env "${test_update_mountpoint}" "${UBOOT_ENV_COPY_CONFIG}" "a" "b" "10"

	create_initialized_ro_blockdevice "${test_update_mountpoint}" "/dev/mtd2" "${UBOOT_ENV_CONFIG}"
	cmp_boot_env "${test_update_mountpoint}" "${UBOOT_ENV_CONFIG}" "a" "b" "0x2"

	chroot "${test_update_mountpoint}" "${COMMAND_UNDER_TEST}" -b > "${test_log_output}"
	assertTrue "If two environment are not consistent and can't be fixed, it's a critical failure" "[ ${?} -eq 1 ]"

	cmp_boot_env "${test_update_mountpoint}" "${UBOOT_ENV_COPY_CONFIG}" "a" "b" "10"
	cmp_boot_env "${test_update_mountpoint}" "${UBOOT_ENV_CONFIG}" "a" "b" "0x2"

	chroot "${test_update_mountpoint}" "${COMMAND_UNDER_TEST}" -s
	assertTrue "If two environment are not consistent and can't be fixed, it's a critical failure" "[ ${?} -eq 1 ]"

	cmp_boot_env "${test_update_mountpoint}" "${UBOOT_ENV_COPY_CONFIG}" "a" "b" "10"
	cmp_boot_env "${test_update_mountpoint}" "${UBOOT_ENV_CONFIG}" "a" "b" "0x2"
}

testUpdateTargetBankWithMainTwoInitializedRO()
{
	printf "test,dual-boot-env\0" > "${test_update_mountpoint}/sys/firmware/devicetree/base/compatible"
	get_config "${test_update_mountpoint}/sys/firmware/devicetree/base/compatible"

	create_initialized_ro_blockdevice "${test_update_mountpoint}" "/dev/mtd2" "${UBOOT_ENV_CONFIG}"
	create_initialized_ro_blockdevice "${test_update_mountpoint}" "/dev/mtd3" "${UBOOT_ENV_COPY_CONFIG}"
	cmp_boot_env "${test_update_mountpoint}" "${UBOOT_ENV_CONFIG}" "a" "b" "0x2"
	cmp_boot_env "${test_update_mountpoint}" "${UBOOT_ENV_COPY_CONFIG}" "a" "b" "0x2"

	chroot "${test_update_mountpoint}" "${COMMAND_UNDER_TEST}" -b > "${test_log_output}"
	assertTrue "Even if environment are RO, getting the target should work" "[ ${?} -eq 0 ]"

	assertEquals "Initialized environment should be used" \
	             "b" \
	             "$(cat "${test_log_output}")"

	cmp_boot_env "${test_update_mountpoint}" "${UBOOT_ENV_COPY_CONFIG}" "a" "b" "0x2"
	cmp_boot_env "${test_update_mountpoint}" "${UBOOT_ENV_CONFIG}" "a" "b" "0x2"

	chroot "${test_update_mountpoint}" "${COMMAND_UNDER_TEST}" -s
	assertFalse "Swapping banks should fail if boot environment are RO" "[ ${?} -eq 0 ]"

	cmp_boot_env "${test_update_mountpoint}" "${UBOOT_ENV_COPY_CONFIG}" "a" "b" "0x2"
	cmp_boot_env "${test_update_mountpoint}" "${UBOOT_ENV_CONFIG}" "a" "b" "0x2"
}
